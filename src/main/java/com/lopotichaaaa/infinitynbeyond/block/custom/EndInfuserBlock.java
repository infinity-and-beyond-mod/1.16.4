package com.lopotichaaaa.infinitynbeyond.block.custom;

import com.lopotichaaaa.infinitynbeyond.block.ModBlockStateProperties;
import com.lopotichaaaa.infinitynbeyond.containers.EndInfuserContainer;
import com.lopotichaaaa.infinitynbeyond.tileentity.EndInfuserTile;
import com.lopotichaaaa.infinitynbeyond.tileentity.ModTileEntities;
import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.FurnaceTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.IIntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeBlockState;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;

public class EndInfuserBlock extends ContainerBlock{

    public static final BooleanProperty INFUSING = ModBlockStateProperties.INFUSING;
    public EndInfuserBlock(Properties properties) {
        super(properties);
        setDefaultState(this.stateContainer.getBaseState().with(INFUSING, Boolean.valueOf(false)));
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(INFUSING);
    }

    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof EndInfuserTile) {
                INamedContainerProvider containerProvider = tileentity.getBlockState().getContainer(worldIn, pos);
                NetworkHooks.openGui(((ServerPlayerEntity) player), containerProvider, tileentity.getPos());
            } else {
                throw new IllegalStateException("Container provider missing");
            }
        }
        return ActionResultType.SUCCESS;
    }



    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return ModTileEntities.END_INFUSER_TILE.get().create();
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }


    @Nullable
    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return new EndInfuserTile();
    }
}
