package com.lopotichaaaa.infinitynbeyond.containers;

import com.lopotichaaaa.infinitynbeyond.InfinityAndBeyond;
import com.lopotichaaaa.infinitynbeyond.tileentity.EndInfuserTile;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModContainers {
    public static DeferredRegister<ContainerType<?>> CONTAINERS
            = DeferredRegister.create(ForgeRegistries.CONTAINERS, InfinityAndBeyond.MOD_ID);

    public static final RegistryObject<ContainerType<EndInfuserContainer>> END_INFUSER_CONTAINER
            = CONTAINERS.register("end_infuser_container",
            ()-> IForgeContainerType.create(((windowId, inv, data) -> {
                BlockPos pos = data.readBlockPos();
                World world = inv.player.getEntityWorld();
                EndInfuserTile tile = (EndInfuserTile) world.getTileEntity(pos);
                return new EndInfuserContainer(windowId,world,pos,inv,inv.player,tile.getData());
            })));

    public static void register(IEventBus eventBus){
        CONTAINERS.register(eventBus);
    }
}
