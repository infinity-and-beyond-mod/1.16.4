package com.lopotichaaaa.infinitynbeyond.tileentity;

import com.lopotichaaaa.infinitynbeyond.block.custom.EndInfuserBlock;
import com.lopotichaaaa.infinitynbeyond.containers.EndInfuserContainer;
import com.lopotichaaaa.infinitynbeyond.data.recipes.EndInfusionRecipe;
import com.lopotichaaaa.infinitynbeyond.data.recipes.ModRecipeTypes;
import com.lopotichaaaa.infinitynbeyond.item.ModItems;
import com.lopotichaaaa.infinitynbeyond.utils.ModTags;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.LockableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

public class EndInfuserTile extends LockableTileEntity implements ITickableTileEntity {

    private final ItemStackHandler itemHandler = createHandler();
    private final LazyOptional<IItemHandler> handler = LazyOptional.of(()->itemHandler);
    protected NonNullList<ItemStack> items = NonNullList.withSize(2, ItemStack.EMPTY);
    private int infusionTime = 0;
    private int infusionTimeTotal =1;
    private ItemStack infusingItem;

    protected final IIntArray data = new IIntArray() {
        @Override
        public int get(int index) {
            switch(index){
                case 0:
                    return EndInfuserTile.this.infusionTime;
                case 1:
                    return EndInfuserTile.this.infusionTimeTotal;
                default:
                    return 0;
            }
        }

        @Override
        public void set(int index, int value) {

        }

        @Override
        public int size() {
            return 2;
        }
    };



    public IIntArray getData(){
        return data;
    }

    public EndInfuserTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public EndInfuserTile(){
        this(ModTileEntities.END_INFUSER_TILE.get());
    }

    private ItemStackHandler createHandler() {
        return new ItemStackHandler(2){
            @Override
            protected void onContentsChanged(int slot) {
                markDirty();
            }

            @Override
            public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                switch (slot){
                    case 0: return stack.getItem() == ModItems.END_ESSENCE.get();
                    case 1: return stack.getItem().isIn(ModTags.Items.INFUSABLE);
                    default:
                        return false;
                }
            }

            @Override
            public int getSlotLimit(int slot) {
                switch (slot) {
                    case 0:
                        return 64;
                    case 1:
                        return 1;
                    default:
                        return 0;
                }
            }

            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if(!isItemValid(slot,stack)){
                    return stack;
                }
                return super.insertItem(slot,stack,simulate);
            }
        };


    }

    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        itemHandler.deserializeNBT(nbt.getCompound(("inv")));
        super.read(state, nbt);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put("inv",itemHandler.serializeNBT());
        return super.write(compound);
    }

    @Override
    protected ITextComponent getDefaultName() {
        return new TranslationTextComponent("screen.infinitynbeyond.end_infuser");
    }

    @Override
    protected Container createMenu(int id, PlayerInventory player) {
        return new EndInfuserContainer(id, player.player.world, pos, player, player.player, data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if(cap== CapabilityItemHandler.ITEM_HANDLER_CAPABILITY){
            return handler.cast();
        }

        return super.getCapability(cap);
    }

    public void craft(){
        Inventory inv = new Inventory(itemHandler.getSlots());
        for (int i =0;i<itemHandler.getSlots();i++){
            inv.setInventorySlotContents(i,itemHandler.getStackInSlot(i));
        }
        Optional<EndInfusionRecipe> recipe = world.getRecipeManager()
                .getRecipe(ModRecipeTypes.END_INFUSION_RECIPE,inv,world);

        recipe.ifPresent(iRecipe -> {
            ItemStack output = iRecipe.getRecipeOutput();

            infuseTheItem(output,iRecipe.getInfusionTime());

            markDirty();
        });
    }

    private void infuseTheItem(ItemStack output,int time) {
        infusionTime = time;
        infusionTimeTotal = time;
        world.setBlockState(pos,this.world.getBlockState(this.pos).with(EndInfuserBlock.INFUSING,true));
        itemHandler.extractItem(0,1,false);

        infusingItem = output;
    }
    public boolean isInfusing(){
        return infusionTime > 0 && infusingItem != null;
    }
    @Override
    public void tick() {
        if (world.isRemote){
            return;
        }
        if (infusionTime == 0){
            if (infusingItem == null){
                craft();
            } else {
                world.setBlockState(pos,this.world.getBlockState(this.pos).with(EndInfuserBlock.INFUSING,false));
                itemHandler.extractItem(1,1,false);
                itemHandler.insertItem(1, infusingItem,false);
                infusingItem = null;
            }
        } else {
            infusionTime--;
        }

    }


    public int getSizeInventory() {
        return this.items.size();
    }


    @Override
    public boolean isEmpty() {
        for(ItemStack itemstack : this.items) {
            if (!itemstack.isEmpty()) {
                return false;
            }
        }

        return true;
    }


    @Override
    public ItemStack getStackInSlot(int index) {
        return this.items.get(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.items, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.items, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        ItemStack itemstack = this.items.get(index);
        boolean flag = !stack.isEmpty() && stack.isItemEqual(itemstack) && ItemStack.areItemStackTagsEqual(stack, itemstack);
        this.items.set(index, stack);
        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }


    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        if (this.world.getTileEntity(this.pos) != this) {
            return false;
        } else {
            return player.getDistanceSq((double)this.pos.getX() + 0.5D, (double)this.pos.getY() + 0.5D, (double)this.pos.getZ() + 0.5D) <= 64.0D;
        }
    }

    @Override
    public void clear() {
        this.items.clear();
    }
}
