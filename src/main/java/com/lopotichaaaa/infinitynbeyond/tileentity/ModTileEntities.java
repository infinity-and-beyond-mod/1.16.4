package com.lopotichaaaa.infinitynbeyond.tileentity;


import com.lopotichaaaa.infinitynbeyond.InfinityAndBeyond;
import com.lopotichaaaa.infinitynbeyond.block.ModBlocks;
import net.minecraft.tileentity.FurnaceTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModTileEntities {

    public static DeferredRegister<TileEntityType<?>> TILE_ENTITIES =
        DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, InfinityAndBeyond.MOD_ID);

    public static RegistryObject<TileEntityType<EndInfuserTile>> END_INFUSER_TILE =
            TILE_ENTITIES.register("end_infuser_tile",()->TileEntityType.Builder.create(
                    EndInfuserTile::new,ModBlocks.END_INFUSER_BLOCK.get()).build(null));

    public static void register(IEventBus enventBus){
        TILE_ENTITIES.register(enventBus);
    }

}
